# EGS_ATM_SERVICE

## Introduction

This spring-boot project is responsible for handling all the operations related to ATM Simulator. these operations
include:

* deposit/withdraw/check balance
* check validation of card

## Note
This ATM simulator use 2 type of Token. first when card validation successfully happen it will return a 'ROLE_VERIFIED' token.
this token is used to access the 'check-password' path and if check-password successfully happen it will return a 'ROLE_AUTHENTICATED' token.
'ROLE_AUTHENTICATED' must be used to access other functionality.

## Dependencies to External Services

This service is depending on the following services for full functionality:
in order to communicate with this service there is a username, password in config file.

|Service|Description|
|-------|-----------|
|egs-bank-service|most of the operations on atm-service end up with calling bank-service.

## Application Properties
All important and critical data must be placed in "SECRET_FILE" with the same name of image-name in kubernetes environment.

|Property Name|Default Value|Mapped Environment Variable|Description|
|-------------|-------------|---------------------------|-----------|
|spring.profiles.active|-|ACTIVE_PROFILE|it's use in development mode. you can set 'dev' in development|
|CI-FILES|-|SIT_UAT_PROD|you can use 'sit','uat','prod' in order to deploy in different environment|
|spring.kafka.bootstrap-servers|-|KAFKA_BOOTSTRAP_SERVERS|list of kafka bootstrap servers delimited by `,`|
|spring.kafka.consumer.properties.partition.assignment.strategy|org.apache.kafka.clients.consumer.RoundRobinAssignor|-|read runtime considerations above|
|spring.application.name|egs-atm-service|SPRING_APPLICATION_NAME|-|
|spring.image.name|-|IMAGE_NAME|the image-name string logged in the logstash entries. this will be injected during the ci/cd operations|
|spring.logstash.server|-|LOGSTASH_SERVER|logstash server address|
|spring.logstash.port|-|LOGSTASH_PORT|logstash server port|
|server.port|-|SERVER_PORT|server port|
|bank_service.timeouts.read|10|BANK_SERVICE_TIMEOUTS_READ|sets the read timeout for HTTP GET/POST/PUT/DELETE requests|
|bank_service.timeouts.connection|3|BANK_SERVICE_TIMEOUTS_CONNECTION|sets the connection timeout for HTTP requests|
|logging.level.ROOT|-|LOG_LEVEL|defines the root log level. suggested value: `INFO`|
|management.endpoints.web.base-path|/|MANAGEMENT_ENDPOINTS_WEB_BASE-PATH|management APIs will be served under this path|
|management.endpoints.web.exposure.include|health, prometheus|MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE|defines the information to be exposed via this endpoint|
|management.server.port|-|MANAGEMENT_SERVER_PORT|defines the management server port. health and metrics APIs will be available in this port|
|security.basic.enabled|false|-|basic security is disabled over egs-service|
|security.sessions|stateless|-|-|
|security.user.password|none|-|-|
|security.jwt.base| TRUE |JWT TOKEN use in order to Authenticate and Authorize users|