package com.egs.atmservice.service;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.ResponseDto;
import com.egs.atmservice.dto.TransferDto;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
@RequiredArgsConstructor
public class BankServiceImp implements BankService {

    private final RestTemplate restTemplate;

    private static final String BANK_SERVICE_BASE_PATH = "/api/v1/credit-card/{cardNumber}";
    private static final String BANK_SERVICE = "bank_service";

    @Override
    @Retry(name = BANK_SERVICE)
    @CircuitBreaker(name = BANK_SERVICE)
    public Double getCreditCardBalance(String cardNumber) {

        ResponseDto response = restTemplate.getForEntity(BANK_SERVICE_BASE_PATH + "/get-balance", ResponseDto.class, cardNumber).getBody();

        return (Double) response.getData();
    }

    @Override
    @Retry(name = BANK_SERVICE)
    @CircuitBreaker(name = BANK_SERVICE)
    public void deposit(String cardNumber, TransferDto transferDto) {

        restTemplate.put(BANK_SERVICE_BASE_PATH + "/deposit", ResponseDto.class, transferDto, cardNumber);
    }

    @Override
    @Retry(name = BANK_SERVICE)
    @CircuitBreaker(name = BANK_SERVICE)
    public void withdraw(String cardNumber, TransferDto transferDto) {

        restTemplate.put(BANK_SERVICE_BASE_PATH + "/withdraw", ResponseDto.class, transferDto, cardNumber);
    }

    @Override
    @Retry(name = BANK_SERVICE)
    @CircuitBreaker(name = BANK_SERVICE)
    public boolean checkPassword(String cardNumber, String password) {
        ResponseDto response = restTemplate.getForEntity(BANK_SERVICE_BASE_PATH + "/check-password?password=", ResponseDto.class, cardNumber, password).getBody();

        return (boolean) response.getData();
    }

    @Override
    @Retry(name = BANK_SERVICE)
    @CircuitBreaker(name = BANK_SERVICE)
    public void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto) {
        restTemplate.put(BANK_SERVICE_BASE_PATH + "/prefer-auth", ResponseDto.class, preferAuthMethodDto, cardNumber);
    }

}
