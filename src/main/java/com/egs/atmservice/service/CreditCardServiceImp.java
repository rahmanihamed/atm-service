package com.egs.atmservice.service;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.TransferDto;
import com.egs.atmservice.exception.InvalidCardException;
import com.egs.atmservice.utility.JWTUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CreditCardServiceImp implements CreditCardService {

    private final BankService bankService;
    private final JWTUtility jwtUtility;

    @Override
    public String validateCardNumber(String cardNumber) throws InvalidCardException {
        if (cardNumber.length() == 16)
            return jwtUtility.generateCardVerificationToken(cardNumber);
        throw new InvalidCardException();
    }

    @Override
    public Double getCreditCardBalance(String cardNumber) {
        return bankService.getCreditCardBalance(cardNumber);
    }

    @Override
    public void deposit(String cardNumber, TransferDto transferDto) {
        bankService.deposit(cardNumber, transferDto);
    }

    @Override
    public void withdraw(String cardNumber, TransferDto transferDto) {
        bankService.withdraw(cardNumber, transferDto);
    }

    @Override
    public String checkPassword(String cardNumber, String password) {
        bankService.checkPassword(cardNumber, password);
        return jwtUtility.generateCardAuthenticateToken(cardNumber);
    }

    @Override
    public void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto) {
        bankService.setPreferAuthMethod(cardNumber, preferAuthMethodDto);
    }
}
