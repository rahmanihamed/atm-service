package com.egs.atmservice.service;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.TransferDto;

public interface BankService {

    Double getCreditCardBalance(String cardNumber);

    void deposit(String cardNumber, TransferDto transferDto);

    void withdraw(String cardNumber, TransferDto transferDto);

    boolean checkPassword(String cardNumber, String password);

    void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto);
}
