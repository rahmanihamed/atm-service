package com.egs.atmservice.service;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.TransferDto;
import com.egs.atmservice.exception.InvalidCardException;

public interface CreditCardService {

    String validateCardNumber(String cardNumber) throws InvalidCardException;

    Double getCreditCardBalance(String cardNumber);

    void deposit(String cardNumber, TransferDto transferDto);

    void withdraw(String cardNumber, TransferDto transferDto);

    String checkPassword(String cardNumber, String password);

    void setPreferAuthMethod(String cardNumber, PreferAuthMethodDto preferAuthMethodDto);
}
