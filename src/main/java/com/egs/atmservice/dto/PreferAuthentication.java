package com.egs.atmservice.dto;

public enum PreferAuthentication {
    PIN, FINGERPRINT
}
