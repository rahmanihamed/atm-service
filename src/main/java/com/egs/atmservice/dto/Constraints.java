package com.egs.atmservice.dto;

public class Constraints {
    public static final String ROLE_VERIFIED = "ROLE_VERIFIED";
    public static final String ROLE_AUTHENTICATED = "ROLE_AUTHENTICATED";
    public static final String ROLE = "role";
}
