package com.egs.atmservice.exception;

public class InvalidCardException extends Exception {
    public InvalidCardException() {
        super("Card Number is not Valid.");
    }
}
