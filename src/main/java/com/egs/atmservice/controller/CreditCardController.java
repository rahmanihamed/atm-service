package com.egs.atmservice.controller;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.ResponseDto;
import com.egs.atmservice.dto.TransferDto;
import com.egs.atmservice.exception.InvalidCardException;
import com.egs.atmservice.service.CreditCardService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/v1/credit-card")
@AllArgsConstructor
@Validated
public class CreditCardController {

    private static final String ATM_SERVICE = "default";

    private final CreditCardService creditCardService;

    @GetMapping("/{cardNumber}/validation")
    @RateLimiter(name = ATM_SERVICE)
    public ResponseEntity<ResponseDto> validateCardNumber(@PathVariable String cardNumber) throws InvalidCardException {

        return ResponseEntity.ok(new ResponseDto().success(creditCardService.validateCardNumber(cardNumber)));
    }

    @GetMapping("/{cardNumber}/get-balance")
    @RateLimiter(name = ATM_SERVICE)
    public ResponseEntity<ResponseDto> getCreditCardBalance(@PathVariable String cardNumber) {

        return ResponseEntity.ok(new ResponseDto().success(creditCardService.getCreditCardBalance(cardNumber)));
    }

    @GetMapping("/{cardNumber}/check-password")
    @RateLimiter(name = ATM_SERVICE)
    public ResponseEntity<ResponseDto> checkPassword(@PathVariable String cardNumber, @RequestParam @NotBlank String password) {

        return ResponseEntity.ok(new ResponseDto().success(creditCardService.checkPassword(cardNumber, password)));
    }

    @PutMapping("/{cardNumber}/deposit")
    @RateLimiter(name = ATM_SERVICE)
    public ResponseEntity deposit(@PathVariable String cardNumber, @RequestBody @Valid TransferDto transferDto) {

        creditCardService.deposit(cardNumber, transferDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }

    @PutMapping("/{cardNumber}/withdraw")
    @RateLimiter(name = ATM_SERVICE)
    public ResponseEntity<ResponseDto> withdraw(@PathVariable String cardNumber, @RequestBody @Valid TransferDto transferDto) {

        creditCardService.withdraw(cardNumber, transferDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }

    @PutMapping("/{cardNumber}/prefer-auth")
    public ResponseEntity<ResponseDto> setPreferAuthMethod(@PathVariable String cardNumber, @RequestBody @Valid PreferAuthMethodDto preferAuthMethodDto) {

        creditCardService.setPreferAuthMethod(cardNumber, preferAuthMethodDto);
        return ResponseEntity.ok(new ResponseDto().success());
    }
}
