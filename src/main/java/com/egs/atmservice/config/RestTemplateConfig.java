package com.egs.atmservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Configuration
public class RestTemplateConfig {

    private final String bankServiceAddress;
    private final String userName;
    private final String password;
    private final Duration connectionTimeout;
    private final Duration readTimeout;

    public RestTemplateConfig(
            @Value("${bank_service.address}") String bankServiceAddress,
            @Value("${bank_service.username}") String userName,
            @Value("${bank_service.password}") String password,
            @Value("${bank_service.timeout.connection}") long connectionTimeout,
            @Value("${bank_service.timeout.read}") long readTimeout) {
        this.userName = userName;
        this.password = password;

        this.connectionTimeout = Duration.of(connectionTimeout, ChronoUnit.SECONDS);
        this.readTimeout = Duration.of(readTimeout, ChronoUnit.SECONDS);
        this.bankServiceAddress = bankServiceAddress;

    }

    @Bean("bank_service")
    public RestTemplate createBankServiceClientRestTemplate() {

        return new RestTemplateBuilder()
                .rootUri(this.bankServiceAddress)
                .setConnectTimeout(this.connectionTimeout)
                .setReadTimeout(this.readTimeout)
                .basicAuthentication(userName, password)
                .build();
    }
}
