package com.egs.atmservice.config;


import com.egs.atmservice.service.UserService;
import com.egs.atmservice.utility.JWTUtility;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.egs.atmservice.dto.Constraints.ROLE_AUTHENTICATED;
import static com.egs.atmservice.dto.Constraints.ROLE_VERIFIED;

@Component
@AllArgsConstructor
public class JwtFilter extends OncePerRequestFilter {

    private final JWTUtility jwtUtility;
    private final UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorization = httpServletRequest.getHeader("Authorization");
        String token = null;
        String userName = null;
        if (null != authorization && (authorization.startsWith("Bearer ") || authorization.startsWith("bearer "))) {
            token = authorization.substring(7);
            userName = jwtUtility.getUsernameFromToken(token);
            String role = jwtUtility.getRolesFromToken(token);

            if (httpServletRequest.getServletPath().contains("check-password") && !role.equals(ROLE_VERIFIED)
                    || !httpServletRequest.getServletPath().contains("check-password") && !role.equals(ROLE_AUTHENTICATED)
            ) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
                return;
            }

        }

        if (null != userName && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails
                    = userService.loadUserByUsername(userName);

            if (jwtUtility.validateToken(token)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                        = new UsernamePasswordAuthenticationToken(userDetails,
                        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest), userDetails.getAuthorities());

                usernamePasswordAuthenticationToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest)
                );

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }

        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
