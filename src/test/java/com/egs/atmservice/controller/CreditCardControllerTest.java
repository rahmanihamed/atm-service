package com.egs.atmservice.controller;

import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.PreferAuthentication;
import com.egs.atmservice.dto.TransferDto;
import com.egs.atmservice.service.CreditCardService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CreditCardControllerTest {

    private final String cardNumber = UUID.randomUUID().toString();
    private final String password = UUID.randomUUID().toString();
    private final Double amount = Double.valueOf(1000);
    private static final ObjectMapper JSON_SERIALIZER = new ObjectMapper();

    private MockMvc mockMvc;
    @Mock
    private CreditCardService cardService;
    @InjectMocks
    private CreditCardController creditCardController;

    @BeforeEach
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(creditCardController)
                .setControllerAdvice(ControllerAdviser.class)
                .build();
    }

    @Test
    void getCreditCardBalance() throws Exception {

        mockMvc.perform(get("/api/v1/credit-card/{cardNumber}/get-balance", cardNumber)
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().is(HttpStatus.OK.value()));
        verify(cardService, times(1)).getCreditCardBalance(cardNumber);

    }

    @Test
    void checkPassword() throws Exception {

        mockMvc.perform(get("/api/v1/credit-card/{cardNumber}/check-password", cardNumber)
                .param("password", password)
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().is(HttpStatus.OK.value()));
        verify(cardService, times(1)).checkPassword(cardNumber, password);

    }

    @Test
    void deposit() throws Exception {

        TransferDto transferDto = new TransferDto(amount);
        mockMvc.perform(put("/api/v1/credit-card/{cardNumber}/deposit", cardNumber)
                .content(JSON_SERIALIZER.writeValueAsString(transferDto))
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().is(HttpStatus.OK.value()));
        verify(cardService, times(1)).deposit(cardNumber, transferDto);

    }

    @Test
    void withdraw() throws Exception {

        TransferDto transferDto = new TransferDto(amount);

        mockMvc.perform(put("/api/v1/credit-card/{cardNumber}/withdraw", cardNumber)
                .content(JSON_SERIALIZER.writeValueAsString(transferDto))
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().is(HttpStatus.OK.value()));
        verify(cardService, times(1)).withdraw(cardNumber, new TransferDto(amount));

    }
    @Test
    void setPreferAuthMethod() throws Exception {

        PreferAuthMethodDto authMethodDto = new PreferAuthMethodDto(PreferAuthentication.PIN);

        mockMvc.perform(put("/api/v1/credit-card/{cardNumber}/prefer-auth", cardNumber)
                .content(JSON_SERIALIZER.writeValueAsString(authMethodDto))
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().is(HttpStatus.OK.value()));
        verify(cardService, times(1)).setPreferAuthMethod(cardNumber, authMethodDto);

    }
}
