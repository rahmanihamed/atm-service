package com.egs.atmservice.service;


import com.egs.atmservice.dto.PreferAuthMethodDto;
import com.egs.atmservice.dto.PreferAuthentication;
import com.egs.atmservice.dto.TransferDto;
import com.egs.atmservice.utility.JWTUtility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreditCardServiceTest {
    private final String cardNumber = UUID.randomUUID().toString();
    private final String password = UUID.randomUUID().toString();
    private final Double amount = Double.valueOf(1000);

    @Mock
    private JWTUtility jwtUtility;
    @Mock
    private BankService bankService;
    @InjectMocks
    private CreditCardServiceImp creditCardService;


    @Test
    void getCreditCardBalance() {

        doReturn(40.00)
                .when(bankService)
                .getCreditCardBalance(cardNumber);

        creditCardService.getCreditCardBalance(cardNumber);

        verify(bankService).getCreditCardBalance(cardNumber);
    }

    @Test
    void checkPassword() throws Exception {

        doReturn(true)
                .when(bankService)
                .checkPassword(cardNumber, password);

        creditCardService.checkPassword(cardNumber, password);

        verify(bankService).checkPassword(cardNumber, password);
    }

    @Test
    void deposit() {

        TransferDto transferDto = new TransferDto(amount);

        doNothing().when(bankService).deposit(cardNumber, transferDto);

        creditCardService.deposit(cardNumber, transferDto);

        verify(bankService).deposit(cardNumber, transferDto);
    }

    @Test
    void withdraw() {

        TransferDto transferDto = new TransferDto(amount);

        doNothing().when(bankService).withdraw(cardNumber, transferDto);

        creditCardService.withdraw(cardNumber, transferDto);

        verify(bankService).withdraw(cardNumber, transferDto);
    }

    @Test
    void setPreferAuthMethod() {
        PreferAuthMethodDto authMethodDto = new PreferAuthMethodDto(PreferAuthentication.PIN);

        doNothing().when(bankService).setPreferAuthMethod(cardNumber, authMethodDto);

        creditCardService.setPreferAuthMethod(cardNumber, authMethodDto);

        verify(bankService).setPreferAuthMethod(cardNumber, authMethodDto);
    }
}
